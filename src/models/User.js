
const Promise = require('bluebird')
const bcrypt = Promise.promisifyAll(require('bcrypt-nodejs'))
const { Model, DataTypes } = require('sequelize');

function hashPassword (user, options) {
  const SALT_FACTOR = 8

  if (!user.changed('password')) {
    return
  }

  return bcrypt
    .genSaltAsync(SALT_FACTOR)
    .then(salt => bcrypt.hashAsync(user.password, salt, null))
    .then(hash => {
      user.setDataValue('password', hash)
    })
}

class User extends Model{
    static init(sequelize){
        super.init({
            name: {
                type: DataTypes.STRING,
                allowNull: false,
                
                validate:{
                    notEmpty: {
                        msg: "Campo Nome deve ser preenchido"
                    }
                }
            },
            email: {
                type: DataTypes.STRING,
                allowNull: false,
                unique: true,
                validate:{
                    isEmail: {
                        msg: "Insira um email correto"
                    }
                }
            },
            password: {
                type: DataTypes.STRING,
                allowNull: false,
                validate:{
                    notEmpty: {
                        msg: "Campo Senha deve ser preenchido"
                    }
                },
                
            },
            adm: DataTypes.BOOLEAN,
            
        },{
            sequelize,
            hooks: {
              
              
              beforeSave: hashPassword
            }
          }),
          User.prototype.comparePassword = function (password) {
            return bcrypt.compareAsync(password, this.password)
          }
    }
    // static associate(models){
    //     this.hasMany(models.Address, {foreignKey:'id_user', as: 'addresses'});
    //     this.hasMany(models.Messages, {foreignKey:'id_user_sender', as: 'messageSender'});
    //     this.hasMany(models.Messages, {foreignKey:'id_user_receiver', as: 'messageReceiver'});
    // }
}

module.exports = User;