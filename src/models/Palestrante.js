const Promise = require('bluebird')
const bcrypt = Promise.promisifyAll(require('bcrypt-nodejs'))
const { Model, DataTypes } = require('sequelize');

class palestrantes extends Model{
    static init(sequelize){
        super.init({
            Id: {
                type: DataTypes.UUID,
                allowNull: false,
                defaultValue: DataTypes.UUIDV1,
                primaryKey: true
        
              },
              Nome: {
                type: DataTypes.STRING,
                
              },
              Cargo:{
                type: DataTypes.STRING,
                
                
              },
              
              Empresa:{
                type: DataTypes.STRING,
                
              },
              Biografia:{
                type: DataTypes.STRING,
                
              },
              Foto:{
                type: DataTypes.STRING,
                
                
              },
             
            
        },{
            sequelize,
            
          })
          
    }
    static associate(models){
        this.belongsToMany(models.palestras, {foreignKey:"Id_palestrantes", through:'palestrantes-palestras', as:"palestras"})
    }
    
}

module.exports = palestrantes;