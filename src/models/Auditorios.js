const Promise = require('bluebird')
const bcrypt = Promise.promisifyAll(require('bcrypt-nodejs'))
const { Model, DataTypes } = require('sequelize');

class auditorios extends Model{
    static init(sequelize){
        super.init({
            Id: {
                type: DataTypes.UUID,
                allowNull: false,
                defaultValue: DataTypes.UUIDV1,
                primaryKey: true
        
              },
              Nome: {
                type: DataTypes.STRING,
                
              },
             
             
            
        },{
            sequelize,
            
          })
          
    }
    // static associate(models){
    //     this.belongsToMany(models.palestrantes, {foreignKey:"Id_palestras", through:'palestrantes-palestras', as:"palestrantes"});
    //     this.belongsToMany(models.estados, {foreignKey:"Id_palestra", through:'palestra-estado', as:"estados"})
    // }
    
}

module.exports = auditorios;