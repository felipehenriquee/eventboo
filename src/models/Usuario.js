const Promise = require('bluebird')
const bcrypt = Promise.promisifyAll(require('bcrypt-nodejs'))
const { Model, DataTypes } = require('sequelize');

function hashPassword (user, options) {
  const SALT_FACTOR = 8

  if (!user.changed('password')) {
    return
  }

  return bcrypt
    .genSaltAsync(SALT_FACTOR)
    .then(salt => bcrypt.hashAsync(user.password, salt, null))
    .then(hash => {
      user.setDataValue('password', hash)
    })
}
class usuario extends Model{
    static init(sequelize){
        super.init({
          Id: {
            type: DataTypes.UUID,
            allowNull: false,
            defaultValue: DataTypes.UUIDV1,
            primaryKey: true
    
          },
            Nome: {
                type: DataTypes.STRING,
                
              },
              
              Sobrenome:{
                type: DataTypes.STRING,
                
              },
              NomeCompleto:{
                type: DataTypes.STRING,
                
              },
              Email:{
                type: DataTypes.STRING,
                
              },
              Login:{
                type: DataTypes.STRING,
                
              },
              Senha:{
                type: DataTypes.STRING,
                
              },
              Role:{
                type: DataTypes.STRING,
               
              },
              Oculto:{
                type: DataTypes.STRING,
                
              },
              Ativo:{
                type: DataTypes.STRING,
                
              },
              Token:{
                type: DataTypes.STRING,
                
              },
              FotoUrl:{
                type: DataTypes.STRING,
                
              },
              Telefone:{
                type: DataTypes.STRING,
                
              },
              TipoLogin:{
                type: DataTypes.STRING,
                
              },
              Pais:{
                type: DataTypes.STRING,
               
              },
              Estado:{
                type: DataTypes.STRING,
                
              },
              Cidade:{
                type: DataTypes.STRING,
                
              },
              Empresa:{
                type: DataTypes.STRING,
                
              },
              CNPJ:{
                type: DataTypes.STRING,
                
              },
              DtNascimento:{
                type: DataTypes.DATE,
                
              },
              Genero:{
                type: DataTypes.STRING,
                
              },
              CPF:{
                type: DataTypes.STRING,
                
              },
              Local:{
                type: DataTypes.STRING,
                
              },
            
        },{
          sequelize,
          hooks: {
            
            
            beforeSave: hashPassword
          }
        }),
        usuario.prototype.comparePassword = function (Senha) {
          return bcrypt.compareAsync(Senha, this.Senha)
        }
          
    }
    
}

module.exports = usuario;