const Promise = require('bluebird')
const bcrypt = Promise.promisifyAll(require('bcrypt-nodejs'))
const { Model, DataTypes } = require('sequelize');

class categorias extends Model{
    static init(sequelize){
        super.init({
            Id: {
                type: DataTypes.UUID,
                allowNull: false,
                defaultValue: DataTypes.UUIDV1,
                primaryKey: true
        
              },
              Nome: {
                type: DataTypes.STRING,
                allowNull: false,
              },
              Ativo: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                
              },
              Tipo: {
                type: DataTypes.INTEGER,
                allowNull: false,
              },
             
            
        },{
            sequelize,
            
          })
          
    }
    static associate(models){
        this.belongsToMany(models.estandes, {foreignKey:"Id_categoria", through:'categoriasestandes',})
    }
    
}

module.exports = categorias;