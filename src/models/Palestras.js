const Promise = require('bluebird')
const bcrypt = Promise.promisifyAll(require('bcrypt-nodejs'))
const { Model, DataTypes } = require('sequelize');

class palestras extends Model{
    static init(sequelize){
        super.init({
            Id: {
                type: DataTypes.UUID,
                allowNull: false,
                defaultValue: DataTypes.UUIDV1,
                primaryKey: true
        
              },
              Nome: {
                type: DataTypes.STRING,
                
              },
              Descricao:{
                type: DataTypes.STRING,
                
              },
              LinkPalestra:{
                type: DataTypes.STRING,
                
              },
              Palco:{
                type: DataTypes.STRING,
                
              },
              Estado:{
                type: DataTypes.STRING,
              },
              Data:{
                type: DataTypes.DATE,
                
              },
              HoraInicio:{
                type: DataTypes.DATE,
                
              },
              HoraFim:{
                type: DataTypes.DATE,
               
              },
              Ativo:{
                type: DataTypes.BOOLEAN,
              },
             
            
        },{
            sequelize,
            
          })
          
    }
    static associate(models){
        this.belongsToMany(models.palestrantes, {foreignKey:"Id_palestras", through:'palestrantes-palestras', as:"palestrantes"});
        this.belongsToMany(models.estados, {foreignKey:"Id_palestra", through:'palestra-estado', as:"estados"})
    }
    
}

module.exports = palestras;