const Promise = require('bluebird')
const bcrypt = Promise.promisifyAll(require('bcrypt-nodejs'))
const { Model, DataTypes } = require('sequelize');

class estados extends Model{
    static init(sequelize){
        super.init({
            Id: {
                type: DataTypes.STRING,
                allowNull: false,
                primaryKey: true
              },
              
             
            
        },{
            sequelize,
            
          })
          
    }
    static associate(models){
        this.belongsToMany(models.estandes, {foreignKey:"Id_estado", through:'estandes_estados',});
        this.belongsToMany(models.palestras, {foreignKey:"Id_estado", through:'palestra-estado',});
    }
    
}

module.exports = estados;