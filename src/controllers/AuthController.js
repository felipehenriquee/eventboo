
const AuthService = require('../services/AuthService');
const UsuarioService = require('../services/UsuarioService');
const Validator = require('fastest-validator');
const jwt = require('jsonwebtoken');
const axios = require('axios');
require('dotenv').config()




const v = new Validator()
module.exports = {
    
    async store(req, res){
        const { CPF, Email, Admin, Local} = req.body;
        console.log("email",Email)
        axios.get(`${process.env.API_SYMPLA}/participants?participant_email=${Email}`, {
            headers: {
                's_token': 'db68035cda1b891498290d591f417715292ea7ad4455b70a5d9a23eba444c54e'
            }
        })
        .then(async response => {
            
            if(response.data.data.length>=1){
                const dados = {
                    NomeCompleto: response.data.data[0].first_name + " " + response.data.data[0].last_name, 
                    Email: response.data.data[0].email, 
                    Senha: null , 
                    Telefone: null, 
                    TipoLogin: 0, 
                    Estado: null, 
                    Genero: "não definido", 
                    CPF: null,
                    Role: "admin",
                    CNPJ: null,
                    Empresa: null,
                    Local: Local
                }
                dados.Telefone = response.data.data[0].custom_form[0].value;
                dados.CPF = response.data.data[0].custom_form[1].value;
                dados.CNPJ= response.data.data[0].custom_form[2].value;
                dados.Empresa= response.data.data[0].custom_form[3].value;

                if (dados.CPF == CPF){
                    dados.Senha = CPF;
                    const usuarioService = await UsuarioService.autenticaLogin(dados, false);
                    
                    return res.json({status: 200, result:true, msg: "Email e CPF conferem", idUser: usuarioService.Id});
                    
               }
               else{
                    return res.json({status: 401, result: false, msg: "CPF não confere"});

               }


                
            }
            else{
                return res.json({status: 401, result: false, msg: "Email não cadastrado"});

            }
            
            
            



        })
        .catch(error => {
            console.log(error);
            return res.json(error);
        });
        
    },
}