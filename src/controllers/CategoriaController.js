
const CategoriaService = require('../services/CategoriasService');
const Validator = require('fastest-validator');


const v = new Validator()

module.exports = {
    // listar todos usuarios
    async index(req, res){
        const {pageSize, order, filter, page} = req.query;
        const categorias = await CategoriaService.getAll(pageSize, order, filter, page);
        return res.json(categorias);
    },
    // cadastrar usuario
    async store(req, res){
    
        const categoria = await CategoriaService.newCategoria(req.body);
        
        return res.json(categoria)
 
    },
    // // apaga user
    async delete(req, res){
        const { id_categoria } = req.params;
        console.log(id_categoria)
        const categoria = await CategoriaService.delete(id_categoria);
        console.log("delete",categoria)
        return res.json(categoria)

    },
    async edit(req, res){
        const {id_categoria} = req.params;
        const categoria = await CategoriaService.editCategoria(req.body, id_categoria);
        return res.json(categoria);
    },

    
    
}