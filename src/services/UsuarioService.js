const Usuario = require('../models/Usuario');

module.exports = {
    async autenticaLogin(usuario, bgAdmin){
        console.log("usuario", usuario)
        try {
            const user = await  Usuario.findOne({where:{
                Email: usuario.Email
            }});
            let user2;
            console.log("user2",user)
            if (user==null){
                
                
                user2 = await this.newUser(usuario)
                
                
                return ({status:200, msg: this.verificaAdmin(user2, bgAdmin), Id: user2});
            }
            else{
                console.log("editou", user.Id)
                const teste = await this.editUser(usuario, user.Id);
                console.log(teste)
                console.log(this.verificaAdmin(user))
                return ({status:200, msg: this.verificaAdmin(user, bgAdmin), Id: user.Id});

            }
        } catch (error) {
            return error;
        }
        
    },
    async newUser(dados){
        
        const { NomeCompleto, Email, Senha, Telefone, TipoLogin, Estado, Genero, CPF, Role, CNPJ, Empresa, Local} = dados;
        
        try {
            const user = await Usuario.create( { NomeCompleto, Email, Senha, Telefone, TipoLogin, Estado, Genero, CPF, Role, CNPJ, Empresa, Local} );
            return (user);
        } catch (error) {
            
            
            return error
        }

        
    },
    async editUser(dados, id){
        console.log(id)
        const { NomeCompleto, Email, Senha, Telefone, TipoLogin, Estado, Genero, CPF, Role, CNPJ, Empresa, Local}  = dados;

        try {
            const user = await Usuario.update(
                { NomeCompleto, Email, Senha, Telefone, TipoLogin, Estado, Genero, CPF, Role, CNPJ, Empresa, Local} ,
                {
                    where:{
                        Id: id
                    }
                }
                );
            return ({status:200, user});
        } catch (error) {
            
            
            return error
        }

        
    },
    verificaAdmin(user, bAdmin){
        if (bAdmin && user.Role == "admin" )
        {
            
            return true;
        }
        else if (bAdmin)
        {
           
            
            return false;
        }
        else{
            

            return true;

        }
    },
    async getAll(pageSize = 1000, order = "ASC", filter = "NomeCompleto", page = 0){
        const _pagina = parseInt(page)
        const _tamanho = parseInt(pageSize)
        
        try {
            const result = await Usuario.findAll({
                
                
                order: [
                    [filter, order],
                ],
                limit: _tamanho,
                offset: _pagina*_tamanho
            });
            
            return ({status:200, result:{rows:result}});
        } catch (error) {
            console.log(error)
            return ({status:400, error});
        }
    },
    async isLogado(local, id){
        try {
            const result = await Usuario.findByPk(id);
            console.log(result.Local)
            if (result.Local == local){
                return ({status:200, msg: "pc ok", result: true});
            }
            else{
                return ({status:200, msg: "Só é permitido logar em apenas um aparelho por vez", result: false});

            }
        } catch (error) {
            return ({status:400, msg: error,});
        }
    }
    
    
}