const User = require('../models/User');

module.exports = {
    async getAll(){
        try {
            const users = await  User.findAll();
            return ({status:200, users});

        } catch (error) {
            return error;
        }
        
    },
    async newUser(dados){
        
        const { name, email, password, adm} = dados;

        try {
            const user = await User.create({name, email, password, adm});
            return ({status:200, user});
        } catch (error) {
            
            
            return error
        }

        
    },
    async editUser(dados, id){
        
        const { name, email, password, adm} = dados;

        try {
            const user = await User.update(
                {name, email, password, adm},
                {
                    where:{
                        id: id
                    }
                }
                );
            return ({status:200, user});
        } catch (error) {
            
            
            return error
        }

        
    },
    async getUser(id_user){
        try {
            const user = await User.findByPk(id_user);
            return user;
        } catch (error) {
            return error
        }
    },
    async getUserIsAdmin(isAdmin){
        console.log(isAdmin)
        try {
            const user = await User.findAll({
                attributes: ['id', 'name', 'email'],
                where:{adm: isAdmin}
            });
            return user;
        } catch (error) {
            return error
        }
    },
    async delete(id_user){
        try {
            const user = await User.destroy({
                where: {
                    id: id_user
                }
            });
            return ({status:200, mensagem:"apagado com sucesso"});
        } catch (error) {
            return error
        }
    },
    
    
}