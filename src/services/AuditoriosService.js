const Modelo = require('../models/Auditorios');

module.exports = {
    
    async create(dados){
       
        
        
        
        try {
            const result = await Modelo.create( dados );
        
            
            
            return (result);
        } catch (error) {
            
            
            return error
        }

        
    },
    
    
    async getAll(pageSize = 1000, order = "ASC", filter = "Nome", page = 0){
        const _pagina = parseInt(page)
        const _tamanho = parseInt(pageSize)
        
        try {
            const result = await Modelo.findAll({
                
                
                order: [
                    [filter, order],
                ],
                limit: _tamanho,
                offset: _pagina*_tamanho,
                
                
            });
            
            return ({status:200, result:{rows:result}});
        } catch (error) {
            console.log(error)
            return ({status:400, error});
        }
    },
    async getOne(id){
        
        try {

            const result = await Modelo.findByPk(id)
            console.log(result)
            return ({status:200, result});
        } catch (error) {
            console.log(error)
            return ({status:400, error});
        }
    }
    ,
    async delete(id){
        try {
            const result = await Modelo.destroy({
                where: {
                    Id: id
                }
            });
            return ({status:200, mensagem:"apagado com sucesso"});
        } catch (error) {
            return error
        }
    },
    async edit(dados, id){
        
        try {
            const result = await Modelo.update(
                { dados } ,
                {
                    where:{
                        Id: id
                    }
                }
                );
            return ({status:200, result});
        } catch (error) {
            
            
            return error
        }

        
    },
    
    
}