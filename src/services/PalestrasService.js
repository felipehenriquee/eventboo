const Modelo = require('../models/Palestras');
const PalestrantesModelo = require('../models/Palestrante');
const { Op } = require("sequelize");

module.exports = {
    
    async create(dados){
        try {
            const result = await Modelo.create( dados );
            console.log(dados.Palestrantes, dados.Estados)
            result.setPalestrantes(dados.Palestrantes);
            result.setEstados(dados.Estados);
            return (result);
        } catch (error) {
            
            
            return error
        }

        
    },
    
    
    async getAll(pageSize = 1000, order = "ASC", filter = "Data", page = 0, passou=false){
       
        const _pagina = parseInt(page)
        const _tamanho = parseInt(pageSize)
        
        let where = {}

        if (passou){
            where = {HoraFim: {[Op.gt]: new Date()}}
        }
        
        try {
            const result = await Modelo.findAll({
                
                
                order: [
                    [filter, order],
                ],
                limit: _tamanho,
                offset: _pagina*_tamanho,
                include:[{association:"palestrantes", through:{attributes:[]}}, {association:"estados", through:{attributes:[]}}]
                
            });
            
            
            return ({status:200, result:{rows:result}});
        } catch (error) {
            console.log(error)
            return ({status:400, error});
        }
    },
    async getOne(id){
        
        try {

            const result = await Modelo.findByPk(id, {include:[{association:"palestrantes", through:{attributes:[]}}, {association:"estados", through:{attributes:[]}}]})
            
            return ({status:200, result});
        } catch (error) {
            console.log(error)
            return ({status:400, error});
        }
    }
    ,
    async delete(id){
        try {
            const result = await Modelo.destroy({
                where: {
                    Id: id
                }
            });
            return ({status:200, mensagem:"apagado com sucesso"});
        } catch (error) {
            return error
        }
    },
    async edit(dados, id){
        
        const { Nome, LinkPalestra, Palco, Estados,Data,HoraInicio, HoraFim, Descricao, Palestrantes } = dados;
        try {
            const result = await Modelo.update(
                { Nome,
                    LinkPalestra,
                    Palco,
                    Estados,
                    Data,
                    HoraInicio,
                    HoraFim,
                    Descricao,
                    Palestrantes } ,
                {
                    where:{
                        Id: id
                    }
                }
                );
                
                
                const result2 = await Modelo.findByPk(id)
                
                await result2.setPalestrantes(dados.Palestrantes);
            return ({status:200, result});
        } catch (error) {
            
            
            return error
        }

        
    },
    async validaHoras(dados){
        return true;
    }
    
    
}