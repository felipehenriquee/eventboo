const Categoria = require('../models/Categorias');

module.exports = {
    
    async newCategoria(dados){
        
        const { Nome, Tipo, Ativo} = dados;
        
        try {
            const user = await Categoria.create( { Nome, Tipo, Ativo} );
            return (user);
        } catch (error) {
            
            
            return error
        }

        
    },
    
    
    async getAll(pageSize = 1000, order = "ASC", filter = "Nome", page = 0){
        const _pagina = parseInt(page)
        const _tamanho = parseInt(pageSize)
        
        try {
            const result = await Categoria.findAll({
                
                
                order: [
                    [filter, order],
                ],
                limit: _tamanho,
                offset: _pagina*_tamanho
            });
            
            return ({status:200, result:{rows:result}});
        } catch (error) {
            console.log(error)
            return ({status:400, error});
        }
    },
    async delete(id_categoria){
        try {
            const user = await Categoria.destroy({
                where: {
                    Id: id_categoria
                }
            });
            return ({status:200, mensagem:"apagado com sucesso"});
        } catch (error) {
            return error
        }
    },
    async editCategoria(dados, id){
        
        const { Nome, Tipo, Ativo}  = dados;

        try {
            const user = await Categoria.update(
                { Nome, Tipo, Ativo} ,
                {
                    where:{
                        Id: id
                    }
                }
                );
            return ({status:200, user});
        } catch (error) {
            
            
            return error
        }

        
    },
    
    
}