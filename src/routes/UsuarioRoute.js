const UsuarioController = require("../controllers/UsuarioController");


const express = require('express');
const router = express.Router();

// retorna todos os users
router.post('/AutenticaLogin', UsuarioController.autenticaLogin);
// retorna todos os users
router.get('/getAll', UsuarioController.getAll);
router.get('/local', UsuarioController.isLogado);

// //cadastra user
// router.post('/', UserController.store);

// // retorna um user
// router.get('/:id_user', UserController.indexById);

// router.patch('/:id_user', UserController.editUser);


// router.delete('/:id_user', UserController.delete);

module.exports = router;
