// OBSOLETO

const UserController = require("../controllers/UserController");


const express = require('express');
const router = express.Router();

// retorna todos os users
router.get('/', UserController.index);
// retorna todos os users
router.get('/admin/:admin', UserController.indexByAdm);

//cadastra user
router.post('/', UserController.store);

// retorna um user
router.get('/:id_user', UserController.indexById);

router.patch('/:id_user', UserController.editUser);


router.delete('/:id_user', UserController.delete);

module.exports = router;

