
const express = require('express');
const router = express.Router();
const AuthController = require("../controllers/AuthController");

// // retorna todos os produtos
// router.get('/', ProductsController.index);

// // retorna um produto
// router.get('/:id_product', ProductsController.index);


router.post('/', AuthController.store);


router.patch('/',(req, res, next) =>{
    res.status(201).send({
        mensagem: "Alterado com Sucesso"
    })
});


router.delete('/',(req, res, next) =>{
    res.status(201).send({
        mensagem: "Excluído com Sucesso"
    })
});

module.exports = router;

