const CategoriaController = require("../controllers/CategoriaController");


const express = require('express');
const router = express.Router();

// retorna todos os users
router.post('/', CategoriaController.store);
// retorna todos os users
router.get('/', CategoriaController.index);

// //cadastra user
// router.post('/', UserController.store);

// // retorna um user
// router.get('/:id_user', UserController.indexById);

router.put('/:id_categoria', CategoriaController.edit);


router.delete('/:id_categoria', CategoriaController.delete);

module.exports = router;
