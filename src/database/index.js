const Sequelize = require('sequelize');
const dbConfig = require('../config/database');

const  User = require("../models/User");
const  standes = require("../models/Standes");
const  usuario = require("../models/Usuario");
const  images = require("../models/Images");
const  estandes = require("../models/Standes");
const  categorias = require("../models/Categorias");
const  estados = require("../models/Estados");
const  palestrante = require("../models/Palestrante");
const  palestras = require("../models/Palestras");
const  auditorios = require("../models/Auditorios");



const connection = new Sequelize(dbConfig);

User.init(connection);
standes.init(connection);
usuario.init(connection);
images.init(connection);
estandes.init(connection);
categorias.init(connection);
estados.init(connection);
palestrante.init(connection);
palestras.init(connection);
auditorios.init(connection);


standes.associate(connection.models)
categorias.associate(connection.models)
estados.associate(connection.models)
images.associate(connection.models)
palestras.associate(connection.models)
palestrante.associate(connection.models)



// User.associate(connection.models)


module.exports = connection;