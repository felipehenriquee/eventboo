'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('palestrantes', { 
      Id: {
        type: Sequelize.UUID,
        allowNull: false,
        defaultValue: Sequelize.UUIDV1,
        primaryKey: true

      },
      Nome: {
        type: Sequelize.STRING,
        
      },
      Cargo:{
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
      },
      
      Empresa:{
        type: Sequelize.STRING,
        
      },
      Biografia:{
        type: Sequelize.STRING,
        
      },
      Foto:{
        type: Sequelize.UUID,
        allowNull: false,
        defaultValue: Sequelize.UUIDV1,
      },
      CreatedAt:{
        type: Sequelize.DATE,
        allowNull: false,
      },
      UpdatedAt:{
        type: Sequelize.DATE,
        allowNull: false,
      }

    });
     
  },

  down: async (queryInterface, Sequelize) => {
    
    await queryInterface.dropTable('palestrantes');
     
  }
};
