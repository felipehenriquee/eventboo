'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('categorias', { 
      Id: {
        type: Sequelize.UUID,
        allowNull: false,
        defaultValue: Sequelize.UUIDV1,
        primaryKey: true

      },
      Nome: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      Ativo: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        
      },
      Tipo: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      CreatedAt:{
        type: Sequelize.DATE,
        allowNull: false,
      },
      UpdatedAt:{
        type: Sequelize.DATE,
        allowNull: false,
      }

    });
     
  },

  down: async (queryInterface, Sequelize) => {
    
    await queryInterface.dropTable('categorias');
     
  }
};
