'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    
    await queryInterface.createTable('evento', { 
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,

      },
      nome: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      ativo: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        
      },
      descricao:{
        type: Sequelize.STRING,
        allowNull: false,
      },
      site:{
        type: Sequelize.STRING,
        allowNull: false,
      },
      email:{
        type: Sequelize.STRING,
        allowNull: false,
      },
      facebook:{
        type: Sequelize.STRING,
        allowNull: false,
      },
      instagram:{
        type: Sequelize.STRING,
        allowNull: false,
      },
      linkYoutube:{
        type: Sequelize.STRING,
        allowNull: false,
      },
      logoEvento:{
        type: Sequelize.STRING,
        allowNull: false,
      },
      videoEvento:{
        type: Sequelize.STRING,
        allowNull: false,
      },
      createdAt:{
        type: Sequelize.DATE,
        allowNull: false,
      },
      updatedAt:{
        type: Sequelize.DATE,
        allowNull: false,
      }

    });
     
  },

  down: async (queryInterface, Sequelize) => {
    
    await queryInterface.dropTable('evento');
     
  }
};
