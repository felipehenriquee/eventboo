'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('palestra-estado', { 
      
      Id_palestra: {
        type: Sequelize.UUID,
        allowNull: false,
        defaultValue: Sequelize.UUIDV1,
        references: { model: "palestras", key:"Id"},
        onDelete: 'CASCADE',

      },
      Id_estado: {
        type: Sequelize.STRING,
        allowNull: false,
        references: { model: "estados", key:"Id"},
        onDelete: 'CASCADE',

      },
      
      CreatedAt:{
        type: Sequelize.DATE,
        allowNull: false,
      },
      UpdatedAt:{
        type: Sequelize.DATE,
        allowNull: false,
      }

    });
     
  },

  down: async (queryInterface, Sequelize) => {
    
    await queryInterface.dropTable('palestra-estado');
     
  }
};
