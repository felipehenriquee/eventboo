'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('estandes', { 
      Id: {
        type: Sequelize.UUID,
        allowNull: false,
        defaultValue: Sequelize.UUIDV1,
        primaryKey: true

      },
      Nome: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      Modelo: {
        type: Sequelize.STRING,
        allowNull: false,
        
      },
      Twitter: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      Youtube: {
        type: Sequelize.STRING,
        allowNull: false,
        
      },
      Instagram: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      Catálogo: {
        type: Sequelize.STRING,
        allowNull: false,
        
      },
      Wpp: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      Facebook: {
        type: Sequelize.STRING,
        allowNull: false,
        
      },
      Site: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      Portfólio: {
        type: Sequelize.STRING,
        allowNull: false,
        
      },
      CreatedAt:{
        type: Sequelize.DATE,
        allowNull: false,
      },
      UpdatedAt:{
        type: Sequelize.DATE,
        allowNull: false,
      }

    });
     
  },

  down: async (queryInterface, Sequelize) => {
    
    await queryInterface.dropTable('estandes');
     
  }
};
