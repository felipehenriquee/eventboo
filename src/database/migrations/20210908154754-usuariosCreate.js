'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    
    await queryInterface.createTable('usuarios', { 
      Id: {
        type: Sequelize.UUID,
        allowNull: false,
        defaultValue: Sequelize.UUIDV1,
        primaryKey: true

      },
      Nome: {
        type: Sequelize.STRING,
        
      },
      
      Sobrenome:{
        type: Sequelize.STRING,
        
      },
      NomeCompleto:{
        type: Sequelize.STRING,
        allowNull: false,
      },
      Email:{
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
      },
      Login:{
        type: Sequelize.STRING,
        
      },
      Senha:{
        type: Sequelize.STRING,
        allowNull: false,
      },
      Role:{
        type: Sequelize.STRING,
       
      },
      Oculto:{
        type: Sequelize.STRING,
        
      },
      Ativo:{
        type: Sequelize.STRING,
        
      },
      Token:{
        type: Sequelize.STRING,
        
      },
      FotoUrl:{
        type: Sequelize.STRING,
        
      },
      Telefone:{
        type: Sequelize.STRING,
        
      },
      TipoLogin:{
        type: Sequelize.STRING,
        
      },
      Pais:{
        type: Sequelize.STRING,
       
      },
      Estado:{
        type: Sequelize.STRING,
        
      },
      Cidade:{
        type: Sequelize.STRING,
        
      },
      DtNascimento:{
        type: Sequelize.DATE,
        
      },
      Genero:{
        type: Sequelize.STRING,
        allowNull: false,
      },
      CPF:{
        type: Sequelize.STRING,
        unique: true,
      },
      CreatedAt:{
        type: Sequelize.DATE,
        allowNull: false,
      },
      UpdatedAt:{
        type: Sequelize.DATE,
        allowNull: false,
      }

    });
     
  },

  down: async (queryInterface, Sequelize) => {
    
    await queryInterface.dropTable('usuarios');
     
  }
};

