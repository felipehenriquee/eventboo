'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('palestras', { 
      Id: {
        type: Sequelize.UUID,
        allowNull: false,
        defaultValue: Sequelize.UUIDV1,
        primaryKey: true

      },
      Nome: {
        type: Sequelize.STRING,
        
      },
      Descricao:{
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
      },
      LinkPalestra:{
        type: Sequelize.STRING,
        
      },
      Palco:{
        type: Sequelize.STRING,
        
      },
      Estado:{
        type: Sequelize.STRING,
      },
      Data:{
        type: Sequelize.DATE,
        
      },
      HoraInicio:{
        type: Sequelize.STRING,
        allowNull: false,
      },
      HoraFim:{
        type: Sequelize.STRING,
       
      },
      Ativo:{
        type: Sequelize.BOOLEAN,
      },
      CreatedAt:{
        type: Sequelize.DATE,
        allowNull: false,
      },
      UpdatedAt:{
        type: Sequelize.DATE,
        allowNull: false,
      }

    });
     
  },

  down: async (queryInterface, Sequelize) => {
    
    await queryInterface.dropTable('palestras');
     
  }
};
