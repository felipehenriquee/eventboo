const express = require('express');
const morgan = require('morgan');
const jwt = require('jsonwebtoken');
var cors = require('cors')



const rotaUser = require('./src/routes/UserRoute');
const rotaAuth = require('./src/routes/AuthRoute');
const rotaUsuario = require('./src/routes/UsuarioRoute');
const rotaImage = require('./src/routes/ImageRoute');
const rotaCategoria = require('./src/routes/CategoriaRoute');
const rotaEstande = require('./src/routes/EstandeRoute');
const rotaPalestrante = require('./src/routes/PalestranteRoute');
const rotaPalestras = require('./src/routes/PalestrasRoute');
const rotaAuditorios = require('./src/routes/AuditoriosRoute');


require('./src/database');
const app = express();

app.use(morgan('dev'));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

var corsOptions = {
    origin: '*',
    optionsSuccessStatus: 200,
    accessControlAllowMethods: 'PUT, POST, PATCH, DELETE, GET'
  }

app.use(cors(corsOptions))

// app.use((req, res, next) => {
//     res.header('Access-Control-Allow-Origin', '*');
//     res.header(
//         'Access-Control-Allow-Header',
//         'Origin, X-Requested-With, Content-Type, Accept, Authorization'
//     );

//     if (req.method === 'OPTIONS') {
//         res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
//         return res.status(200).send({});
//     }
//     next();
// });


app.use('/v1/user', rotaUser);
app.use('/v1/auth', rotaAuth);
app.use('/v1/usuario', rotaUsuario);
app.use('/v1/image', rotaImage);
app.use('/v1/categoria', rotaCategoria);
app.use('/v1/estande', rotaEstande);
app.use('/v1/palestrante', rotaPalestrante);
app.use('/v1/palestra', rotaPalestras);
app.use('/v1/auditorio', rotaAuditorios);

// rota not found
app.use((req, res, next)=>{
    const erro = new Error("Não encontrado");
    erro.status = 404 ;
    next(erro);
})

app.use((error, req, res, next) =>{
    res.status(error.status || 500);
    return res.send({
        erro:{
            mensagem: error.message
        }
    })
})
module.exports = app;